from dataclasses import dataclass

@dataclass
class DataRequests(object):
    
    wsStart  = {
        "name":"fromClient",
        "data":
        {
            "case":"start",
        }
    }
    
    moveRight = {
        "name":"fromClient",
        "data":
        {
            "case":"controlCMDAbs",
            "position": {
                "pan": [5],
                "tilt": [5]
            },
            "velocity": {
                "pan": [5],
                "tilt": [5]
            }
        }
    }
    
    moveLeft = {
        "name":"fromClient",
        "data":
        {
            "case":"controlCMDAbs",
            "position": {
                "pan": [-5],
                "tilt": [-5]
            },
            "velocity": {
                "pan": [5],
                "tilt": [5]
            }
        }
    }
    
    poseConfig = {
        "state": True,
        "type": "small",
        "rays": True,
        "feed": False,
        "topLeft": [0, 0],
        "botRight": [640, 480]
    }
    
    configMotion = {
        "panAccel": 1,
        "panDeaccel": 1,
        "tiltAccel": 1,
        "tiltDeaccel": 1,
        "panMax": 350,
        "panMin": -350,
        "tiltMax": 30,
        "tiltMin": -30
    }
    
    home =  {
        "pan": True,
        "tilt": True
    }
    
    moveLeftReq = {
        "position": {
            "pan": [-1],
            "tilt": [-1]
        },
        "velocity": {
            "pan": [1],
            "tilt": [1]
        }
    }
    
    moveRightReq = {
        "position": {
            "pan": [1],
            "tilt": [1]
        },
        "velocity": {
            "pan": [1],
            "tilt": [1]
        }
    }
    
    """API endpoints
    """
    poseURL = 'http://%s:2200/v1/ai/pose/start'
    getposeURL = "http://%s:2200/v1/imaging/pose"
    getImageURL = 'http://%s:2200/v1/imaging/cam'
    putAbsMove = 'http://%s:2200/v1/direct-control/move-absolute'
    getStatus = 'http://%s:2200/v1/system/status'
    getVersion = 'http://%s:2200/v1/system/info'
    
    """web socket endpointss
    """
    imageStreamHost = 'ws://%s:2101/ws'
    poseImageStreamHost = 'ws://%s:2102/ws'
    
    tfCameraTest = True
    tfMotionTest = True
    
    