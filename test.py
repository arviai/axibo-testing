from dataRequests import DataRequests as dr
from loguru import logger
import loguru
import websocket
import json
import _thread as thread
import time
import requests
import sys
from PIL import Image
from io import BytesIO
import base64
import threading
from datetime import datetime
import random

class AxTestUtility(object):
    """a utility that can be used to test an axibos functionality iterativley

    Args:
        object ([type]): [description]
    """
    
    def __init__(self, targetIp):
        self.ip = targetIp
        self.currentTestCycle = 0
        log_format = loguru._defaults.LOGURU_FORMAT
        logger.configure(
            handlers=[{"sink": sys.stderr, "format": log_format}]
        )
        logger.add("axibo_test_" + str(datetime.now().strftime("%m_%d_%Y_%H")) + ".log", rotation="500 MB") 
        self.fpsCounter = 0
    
    def wsOnMessage(self, ws, message):
        self.fpsCounter += 1
    
    def wsOnError(self, ws, error):
        logger.error("Websocket error on %s" % self.ip)
        logger.error(str(error))
        
    def wsOnClose(self, ws):
        pass
    
    @logger.catch    
    def wsOnOpen(self, ws, secondsToRun: int):
        def run(*args):
            for i in range(secondsToRun):
                time.sleep(1)
                try:
                    ws.send(json.dumps(dr.wsStart))
                except Exception as e:
                    logger.error(self.ip + " Websocket error: " + str(e))
            ws.close()
        thread.start_new_thread(run, ())
        
    def testImageFeed(self, secondsToRun=10):
        self.fpsCounter = 0
        ws = websocket.WebSocketApp(
            dr.imageStreamHost % self.ip,
            on_message=self.wsOnMessage, 
            on_error=self.wsOnError,
            on_close=self.wsOnClose
        )
        logger.debug("Starting image feed test")
        self.wsOnOpen(ws, secondsToRun)
        ws.run_forever()
        logger.debug(self.ip + " Grabbing image frame at:" + str(self.fpsCounter/secondsToRun))
        ws.keep_running = False
        
    def testPoseFeed(self, secondsToRun=10):
        self.fpsCounter = 0
        ws = websocket.WebSocketApp(
            dr.poseImageStreamHost % self.ip,
            on_message=self.wsOnMessage, 
            on_error=self.wsOnError,
            on_close=self.wsOnClose
        )
        logger.debug(self.ip + " Starting pose image feed test")
        self.wsOnOpen(ws, secondsToRun)
        ws.run_forever()
        if self.fpsCounter/secondsToRun < 1.0:
            logger.error(self.ip + " Failed to grab image frame at: %s fps" % (self.fpsCounter/secondsToRun))
        else:
            logger.debug(self.ip + " Grabbing image frame at: %s fps" % (self.fpsCounter/secondsToRun))
        ws.keep_running = False 
        
    def enablePosFeed(self):
        dr.poseConfig["feed"] = True
        try:
            poseReq = requests.put(
                dr.poseURL % self.ip,
                data=json.dumps(dr.poseConfig),
                timeout=2).json()
            if not poseReq["message"] == "success":
                logger.error(self.ip + " Failed to enable pose feed " + str(poseReq['message']))
                return
        except Exception as e:
            logger.error(self.ip + " Unable to access API " + str(e))
        time.sleep(5)
        
    def apiGetRawImage(self):
            try:
                data = requests.get(
                    dr.getImageURL % self.ip, 
                    timeout=10)
                getRawImgReq = data.json()
            except Exception as e:
                logger.error(self.ip + " Failed to get image from raw API endpoint " + str(e))
                return
            try:
                if getRawImgReq["Image"] is not None:
                    imgdata = base64.b64decode(str(getRawImgReq["Image"]))
                    image = Image.open(BytesIO(imgdata))
                    image.save(self.ip+".jpg")
                    logger.debug(
                        self.ip + " Grabbing image frame of size: %s %s" 
                        % (str(len(getRawImgReq['Image']) / 1000.0),
                        "k/bytes")
                    )
                else:
                    logger.error(self.ip + " Failed to get image from raw API endpoint, but endpoint returned " + str(data))
            except Exception as e:
                logger.error(self.ip + " Failed to get image from raw API endpoint " + str(e))
                
    def apiGetPoseImage(self):
            try:
                data = requests.get(
                    dr.getposeURL % self.ip, 
                    timeout=10)
                getRawImgReq = data.json()
            except Exception as e:
                logger.error(self.ip + " Failed to get pose image from raw API endpoint " + str(e))
                return
            try:
                if getRawImgReq["Image"] is not None:
                    imgdata = base64.b64decode(str(getRawImgReq["Image"]))
                    image = Image.open(BytesIO(imgdata))
                    image.save(self.ip+".jpg")
                    logger.debug(
                        self.ip + " Grabbing image frame of size: %s %s" 
                        % (str(len(getRawImgReq['Image']) / 1000.0),
                        "k/bytes")
                    )
                else:
                    logger.error(self.ip + " Failed to get image from raw API endpoint, but endpoint returned " + str(data))
            except Exception as e:
                logger.error(self.ip + " Failed to get image from raw API endpoint " + str(getRawImgReq))
                
                
    def apiMoveLeft(self):
            try:
                headers = {'Content-type': 'application/json'}
                ret = requests.put(
                    dr.putAbsMove % self.ip, 
                    data=json.dumps(dr.moveLeftReq), 
                    headers=headers,
                    timeout=5)
            except Exception as e:
                logger.error(self.ip + " Failed to move axibo to the Left " + str(e))
                return
            logger.debug(
                self.ip + " Moved axibo to the left"
            )
            
    def apiMoveRight(self):
            try:
                headers = {'Content-type': 'application/json'}
                ret = requests.put(
                    dr.putAbsMove % self.ip, 
                    data=json.dumps(dr.moveRightReq), 
                    headers=headers,
                    timeout=5)
            except Exception as e:
                logger.error(self.ip + " Failed to move axibo to the Left " + str(e))
                return
            logger.debug(
                self.ip + " Moved axibo to the right"
            )
            
    def apiGetStatus(self):
            try:
                headers = {'Content-type': 'application/json'}
                ret = requests.get(
                    dr.getStatus % self.ip, 
                    timeout=5).json()
            except Exception as e:
                logger.error(self.ip + " Failed to get API status " + str(e))
                return
            logger.debug(
                self.ip + " Got status from the device"
            )

    def getVersion(self):
            try:
                headers = {'Content-type': 'application/json'}
                ret = requests.get(
                    dr.getVersion % self.ip, 
                    timeout=5).json()
            except Exception as e:
                logger.error(self.ip + " Failed to version from device " + str(e))
                return
            logger.debug(
                self.ip + " Got version from device " + str(ret)
            )

if __name__ == '__main__':
    
    def testRoutine(objToTest):
        while 1:
            #objToTest.apiGetStatus()
            objToTest.getVersion()
            
            #bjToTest.apiMoveLeft()
            #time.sleep(0.5)
            #objToTest.apiMoveRight()
            #objToTest.testImageFeed(secondsToRun=3)
            objToTest.apiGetPoseImage()
            #objToTest.apiGetRawImage()
            #quit()
            time.sleep(random.random() * 100)
            #
            #enable the pose feed and sample the pose feed
            #objToTest.enablePosFeed()
            #objToTest.testPoseFeed(secondsToRun=3) 
            #objToTest.apiGetPoseImage()
            #time.sleep(1)
    
    #get the axibos we want to test
    testers = []
    testerThreads = []
    if '--file' in sys.argv:
         with open(sys.argv[2], 'r') as f:
             axibosString = json.load(f)['hostNames']
    
    for axibo in axibosString:
        testers.append(AxTestUtility(axibo))
    
    for idx, tester in enumerate(testers):
        testerThreads.append(threading.Thread(target=testRoutine, args=(tester, )))
        testerThreads[idx].daemon = True
        testerThreads[idx].start()

while 1:
    time.sleep(1000)
    
        