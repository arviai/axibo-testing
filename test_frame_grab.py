import json
import requests
from PIL import Image
from io import BytesIO
import base64
import time

payload={
    "rotation":"0",
    "imgWidth": 2688,
    "imgheight": 1520
}

r = requests.put('http://10.131.25.176:2200/v1/imaging/config', data =json.dumps(payload))
print("Changed camera resolution (2688x1520), waiting for change on device")

time.sleep(10) #Needed for waiting for resolution to change

data = requests.get( 'http://10.131.25.176:2200/v1/imaging/cam', timeout=5).json()

imgdata = base64.b64decode(str(data["Image"]))
image = Image.open(BytesIO(imgdata))
image.save("test.jpg")
print("Saved image as test.jpg")

payload={
    "rotation":"0",
    "imgWidth": 640,
    "imgheight": 480
}

r = requests.put('http://10.131.25.176:2200/v1/imaging/config', data =json.dumps(payload))
print("Changed camera resolution (640x480), waiting for change on device")