## Installation

* Install Python 3.8 (or greater) (can be on a Windows or linux machine)
* Install required packages `pip3 install requests loguru websocket-client

## Usage

This can be used as a module to test individual axibos or many of them. To run the test once against an Axibo cd into the directory and run the following

`python .\test.py --file .\axibos.json --loop`

You can also pass in a hostname or anything else that is a valid network address. This will then produce a log file with something like this in it. 

```log
192.168.2.207  2021-05-26 09:45:49.937 | DEBUG    | __main__:testImageFeed:58 - Starting image feed test
192.168.2.207  2021-05-26 09:45:51.191 | ERROR    | __main__:wsOnError:32 - Websocket error on 192.168.2.207
192.168.2.207  2021-05-26 09:45:51.193 | ERROR    | __main__:wsOnError:33 -
192.168.2.207  2021-05-26 09:45:51.199 | DEBUG    | __main__:testImageFeed:61 - Grabbing image frame at: 11.666666666666666
192.168.2.207  2021-05-26 09:45:51.230 | DEBUG    | __main__:apiGetRawImage:121 - Grabbing image frame of size: 54.14 k/bytes
192.168.2.207  2021-05-26 09:45:51.949 | ERROR    | __main__:run:46 - Websocket error: Connection is already closed.
192.168.2.207  2021-05-26 09:45:52.951 | ERROR    | __main__:run:46 - Websocket error: Connection is already closed.
192.168.2.207  2021-05-26 09:45:56.257 | DEBUG    | __main__:testPoseFeed:72 - Starting pose image feed test
192.168.2.207  2021-05-26 09:45:59.289 | DEBUG    | __main__:testPoseFeed:78 - Grabbing image frame at: 7.0 fps
192.168.2.207  2021-05-26 09:45:59.331 | DEBUG    | __main__:apiGetPoseImage:105 - Grabbing pos image frame of size: 53.516 k/bytes
```

Here is an example of some successes and some failures.